#ifndef BTRX_H
# define BTRX_H

#define GETMARKETS	"https://bittrex.com/api/v1.1/public/getmarkets"
#define GETMARKETHISTORY "https://bittrex.com/api/v1.1/public/getmarkethistory?market="
#define GETMARKETSUMMARY "https://bittrex.com/api/v1.1/public/getmarketsummary?market="
#define GETORDERBOOK	"https://bittrex.com/api/v1.1/public/getorderbook?market="
#define GETTICKER	"https://bittrex.com/api/v1.1/public/getticker?market="
#define MAX_TICKERS	194
#define TICKER_LEN	54
#define MARKETH_LEN	60
#define ORDERB_LEN	62
#define STARTING_TS	1522108800

typedef struct	s_data
{
	char	*data;
	size_t	size;
}		input;

typedef struct	s_market
{
	char	first_chunk;
	int	last_tr_id;
//	int	tr_ids[200];
	double	history[300];
	size_t	curr_idx;
}		t_market;	

typedef struct	s_markets
{
	char		*base_currency;
	char		*name;
	size_t		name_sz;
	t_market	hist;
	struct		s_markets *next;
}			t_market_list;

typedef struct		s_history_data
{
	t_market_list	*market;
	input		*in_buff;
}			t_history_data;

typedef struct		s_candle
{
	unsigned char	open_flag;
	unsigned char	close_flag;
	size_t		trades;
	double		open;
	double		high;
	double		low;
	double		close;
	FILE		*fd;
	double		vol;
}			t_candle;


typedef struct		s_volume
{
	int		last_id;	// last tx id
	int		times;		// timestamp
	int		idx;		// ticker idx
	t_candle	five;		// 5min candles
	t_candle	fifteen;	// 15min candles
	unsigned char	com_five;	// 5min candle progress
	unsigned char	com_fifteen;	// 15min candle progress
}			t_volume;

typedef struct		s_tx_data
{
	double		price[100];
	double		quan[100];
	int		ts[100];
	int		curr_id[100];
}			t_tx_data;

const char tickers[195][20] = 
{
	"BTC-2GIVE",
	"BTC-ABY",
	"BTC-ADA",
	"BTC-ADT",
	"BTC-ADX",
	"BTC-AEON",
	"BTC-AMP",
	"BTC-ANT",
	"BTC-ARDR",
	"BTC-ARK",
	"BTC-AUR",
	"BTC-BAT",
	"BTC-BAY",
	"BTC-BCC",
	"BTC-BCPT",
	"BTC-BCY",
	"BTC-BITB",
	"BTC-BLITZ",
	"BTC-BLK",
	"BTC-BLOCK",
	"BTC-BNT",
	"BTC-BRK",
	"BTC-BRX",
	"BTC-BSD",
	"BTC-BTG",
	"BTC-BURST",
	"BTC-BYC",
	"BTC-CANN",
	"BTC-CFI",
	"BTC-CLAM",
	"BTC-CLOAK",
	"BTC-CLUB",
	"BTC-COVAL",
	"BTC-CPC",
	"BTC-CRB",
	"BTC-CRW",
	"BTC-CURE",
	"BTC-CVC",
	"BTC-DASH",
	"BTC-DCR",
	"BTC-DCT",
	"BTC-DGB",
	"BTC-DMD",
	"BTC-DNT",
	"BTC-DOGE",
	"BTC-DOPE",
	"BTC-DTB",
	"BTC-DYN",
	"BTC-EBST",
	"BTC-EDG",
	"BTC-EFL",
	"BTC-EGC",
	"BTC-EMC",
	"BTC-EMC2",
	"BTC-ENG",
	"BTC-ENRG",
	"BTC-ERC",
	"BTC-ETC",
	"BTC-ETH",
	"BTC-EXCL",
	"BTC-EXP",
	"BTC-FAIR",
	"BTC-FCT",
	"BTC-FLDC",
	"BTC-FLO",
	"BTC-FTC",
	"BTC-GAM",
	"BTC-GAME",
	"BTC-GBG",
	"BTC-GBYTE",
	"BTC-GCR",
	"BTC-GEO",
	"BTC-GLD",
	"BTC-GNO",
	"BTC-GNT",
	"BTC-GOLOS",
	"BTC-GRC",
	"BTC-GRS",
	"BTC-GUP",
	"BTC-HMQ",
	"BTC-IGNIS",
	"BTC-INCNT",
	"BTC-IOC",
	"BTC-ION",
	"BTC-IOP",
	"BTC-KMD",
	"BTC-KORE",
	"BTC-LBC",
	"BTC-LGD",
	"BTC-LMC",
	"BTC-LRC",
	"BTC-LSK",
	"BTC-LTC",
	"BTC-LUN",
	"BTC-MANA",
	"BTC-MCO",
	"BTC-MEME",
	"BTC-MER",
	"BTC-MLN",
	"BTC-MONA",
	"BTC-MUE",
	"BTC-MUSIC",
	"BTC-NAV",
	"BTC-NBT",
	"BTC-NEO",
	"BTC-NEOS",
	"BTC-NLG",
	"BTC-NMR",
	"BTC-NXC",
	"BTC-NXS",
	"BTC-NXT",
	"BTC-OK",
	"BTC-OMG",
	"BTC-OMNI",
	"BTC-PART",
	"BTC-PAY",
	"BTC-PDC",
	"BTC-PINK",
	"BTC-PIVX",
	"BTC-PKB",
	"BTC-POT",
	"BTC-POWR",
	"BTC-PPC",
	"BTC-PTC",
	"BTC-PTOY",
	"BTC-QRL",
	"BTC-QTUM",
	"BTC-QWARK",
	"BTC-RADS",
	"BTC-RBY",
	"BTC-RCN",
	"BTC-RDD",
	"BTC-REP",
	"BTC-RLC",
	"BTC-SALT",
	"BTC-SBD",
	"BTC-SC",
	"BTC-SEQ",
	"BTC-SHIFT",
	"BTC-SIB",
	"BTC-SLR",
	"BTC-SLS",
	"BTC-SNRG",
	"BTC-SNT",
	"BTC-SPHR",
	"BTC-SPR",
	"BTC-SRN",
	"BTC-START",
	"BTC-STEEM",
	"BTC-STORJ",
	"BTC-STRAT",
	"BTC-SWIFT",
	"BTC-SWT",
	"BTC-SYNX",
	"BTC-SYS",
	"BTC-THC",
	"BTC-TIX",
	"BTC-TKS",
	"BTC-TRST",
	"BTC-TRUST",
	"BTC-TRX",
	"BTC-TUSD",
	"BTC-TX",
	"BTC-UBQ",
	"BTC-UKG",
	"BTC-UNB",
	"BTC-VEE",
	"BTC-VIA",
	"BTC-VIB",
	"BTC-VRC",
	"BTC-VRM",
	"BTC-VTC",
	"BTC-VTR",
	"BTC-WAVES",
	"BTC-WAX",
	"BTC-WINGS",
	"BTC-XCP",
	"BTC-XDN",
	"BTC-XEL",
	"BTC-XEM",
	"BTC-XLM",
	"BTC-XMG",
	"BTC-XMR",
	"BTC-XMY",
	"BTC-XRP",
	"BTC-XST",
	"BTC-XVC",
	"BTC-XVG",
	"BTC-XWC",
	"BTC-XZC",
	"BTC-ZCL",
	"BTC-ZEC",
	"BTC-ZEN",
	"BTC-ZRX"
};

#endif

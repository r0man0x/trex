#ifndef _BINANCE_H_
# define _BINANCE_H_

#define EX_INFO			"https://api.binance.com/api/v1/exchangeInfo"
#define EX_KLINES		"https://api.binance.com/api/v1/klines?symbol="
#define EX_AGGTRADE(s, add)	"https://api.binance.com/api/v1/aggTrades?symbol="s""add
#define SYMBOLS			113
#define KLINE_LEN		45
#define INTERVAL		"15m"
#define S_LIMIT			"20"
#define I_LIMIT			20
#define	VOL_LIMIT		2.0 // 200% vol increase


typedef struct	s_candle
{
	double	close;
	double	open;
	double	high;
	double	low;
	double	vol;
	size_t	trades;
}		t_candle;


typedef	struct		s_market
{
	t_candle	fifteen[I_LIMIT];
	size_t		idx;
			
}			t_market;

static char symbols[113][10] = 
{
	"ETHBTC",
	"LTCBTC",
	"BNBBTC",
	"NEOBTC",
	"BCCBTC",
	"GASBTC",
	"HSRBTC",
	"MCOBTC",
	"WTCBTC",
	"LRCBTC",
	"QTUMBTC",
	"YOYOBTC",
	"OMGBTC",
	"ZRXBTC",
	"STRATBTC",
	"SNGLSBTC",
	"BQXBTC",
	"KNCBTC",
	"FUNBTC",
	"SNMBTC",
	"IOTABTC",
	"LINKBTC",
	"XVGBTC",
	"CTRBTC",
	"SALTBTC",
	"MDABTC",
	"MTLBTC",
	"SUBBTC",
	"EOSBTC",
	"SNTBTC",
	"ETCBTC",
	"MTHBTC",
	"ENGBTC",
	"DNTBTC",
	"ZECBTC",
	"BNTBTC",
	"ASTBTC",
	"DASHBTC",
	"OAXBTC",
	"ICNBTC",
	"BTGBTC",
	"EVXBTC",
	"REQBTC",
	"VIBBTC",
	"TRXBTC",
	"POWRBTC",
	"ARKBTC",
	"XRPBTC",
	"MODBTC",
	"ENJBTC",
	"STORJBTC",
	"VENBTC",
	"KMDBTC",
	"RCNBTC",
	"NULSBTC",
	"RDNBTC",
	"XMRBTC",
	"DLTBTC",
	"AMBBTC",
	"BATBTC",
	"BCPTBTC",
	"ARNBTC",
	"GVTBTC",
	"CDTBTC",
	"GXSBTC",
	"POEBTC",
	"QSPBTC",
	"BTSBTC",
	"XZCBTC",
	"LSKBTC",
	"TNTBTC",
	"FUELBTC",
	"MANABTC",
	"BCDBTC",
	"DGDBTC",
	"ADXBTC",
	"ADABTC",
	"PPTBTC",
	"CMTBTC",
	"XLMBTC",
	"CNDBTC",
	"LENDBTC",
	"WABIBTC",
	"TNBBTC",
	"WAVESBTC",
	"GTOBTC",
	"ICXBTC",
	"OSTBTC",
	"ELFBTC",
	"AIONBTC",
	"NEBLBTC",
	"BRDBTC",
	"EDOBTC",
	"WINGSBTC",
	"NAVBTC",
	"LUNBTC",
	"TRIGBTC",
	"APPCBTC",
	"VIBEBTC",
	"RLCBTC",
	"INSBTC",
	"PIVXBTC",
	"IOSTBTC",
	"CHATBTC",
	"STEEMBTC",
	"NANOBTC",
	"VIABTC",
	"BLZBTC",
	"AEBTC",
	"RPXBTC",
	"NCASHBTC",
	"POABTC",
	"ZILBTC"
};

#endif

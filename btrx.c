#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include "btrx.h"
#include <string.h>


unsigned char bot_detect_duplicate_orders(t_market_list *market)
{
	double	ord;
	size_t	idx;
	size_t	x;

	ord = 0l;
	idx = 0;
	
	
	while (idx < market->hist.curr_idx - 1)
	{

		ord = market->hist.history[idx];
		x = idx + 1;
		while (x < market->hist.curr_idx)
		{
			if (ord == market->hist.history[x])
			{
				printf("   [%lf] ", ord);
				return 1;
			}
			x++;
		}
		idx++;
	}
	
	return 0;	
}

size_t get_history(void *buffer, size_t size, size_t nmemb, void *userp)
{
	t_history_data	*in = (t_history_data*)userp;
	char		*buff = (char*)buffer;
	size_t		idx;
	size_t		bracket_pos;
	char		not_enough_data = 0;


	idx = 0;
	if (in->market->hist.first_chunk == 0)	
	{
		if (nmemb < 300 || memcmp(buff + 11, "true", 4) != 0) // read min 300 at a time
		{
			printf("    nmemb < 300 || success == false\n");
			return 0;	
		}
		in->market->hist.first_chunk = 1;
		while (buff[idx] != '[' && idx < nmemb)
			idx++;
		idx++;
		
		bracket_pos = idx + 6;
		while (buff[bracket_pos] != ',' && bracket_pos < nmemb)
			bracket_pos++;
		
		if (idx == nmemb || bracket_pos == nmemb)
			return 0;
		// write last transaction ID
		char	tbuff[20];
		int	tr_id;

		memcpy(tbuff, buff + idx + 6, bracket_pos - 6 - idx);
		tbuff[bracket_pos - 6 - idx] = '\0';
		tr_id = atoi(tbuff);
		if (tr_id != in->market->hist.last_tr_id)
			in->market->hist.last_tr_id = tr_id;
		else
			return 0;
	}

	bracket_pos = 0;
	if (in->in_buff->size != 0) // if the last chunk was not complete, concat with this one 
	{

 		while (buff[bracket_pos] != '}' && bracket_pos < nmemb) 
			bracket_pos++;
		
		char *tbuff;
		
		tbuff = (char*)malloc(sizeof(char) * (in->in_buff->size + bracket_pos + 1));
		memcpy(tbuff, in->in_buff->data, in->in_buff->size);
		memcpy(tbuff, buff, bracket_pos);
		in->in_buff->size += bracket_pos;

		idx = 0;
		while (buff[idx] != 'Q' && idx < in->in_buff->size)
			idx++;
		if (buff[idx] == 'Q')
		{
			idx += 11;
			size_t temp = idx;

			while (buff[temp] != ',' && temp < in->in_buff->size)
				temp++;
			if (buff[temp] == ',')
			{
				char tbuf[20];
				memcpy(tbuf, buff + idx, temp - idx - 1);
				tbuf[temp - idx] = '\0';
				in->market->hist.history[in->market->hist.curr_idx] = atof(tbuf);	
				in->market->hist.curr_idx++;
			}
		}
	
		free(tbuff);
		idx = bracket_pos + 1;
		bracket_pos = 0;
		idx = 0;
		
	}

	while (idx < nmemb)
	{
		if (buff[idx] == '{')
		{
			bracket_pos = idx;
			while (buff[idx] != '}')
			{
				if (buff[idx] == 'Q')
				{
					if (nmemb - idx < 8)
						not_enough_data = 1;
					else
					{
						idx += 10;
						size_t temp = idx;
						while (buff[temp] != ',' && temp < nmemb)
							temp++;
						if (temp == nmemb)
							not_enough_data = 1;
						else
						{
							char tbuff[50];
							memcpy(tbuff, buff + idx, temp - idx);
							tbuff[temp - idx] = '\0';
							in->market->hist.history[in->market->hist.curr_idx] = atof(tbuff);	
							in->market->hist.curr_idx++;
							
							//printf("\n%s", tbuff);

							if (in->market->hist.curr_idx >= 300)
							{
								in->market->hist.curr_idx = 0;
								printf("  double array idx >= 300\n");
							}
						}
						
					}
					
				}
				
				if (not_enough_data || idx + 1 == nmemb )
				{
					if (in->in_buff->data)
					{
						free(in->in_buff->data);
						in->in_buff->data = NULL;
					}

					in->in_buff->size = nmemb - bracket_pos;
					in->in_buff->data = (char*)malloc(sizeof(char) * in->in_buff->size);
					memcpy(in->in_buff->data, buff + bracket_pos, in->in_buff->size);

					return nmemb;
				}	
				idx++;
			}

		}
		if (buff[idx] == ']')
			return nmemb;

		idx++;
	}
	if (in->in_buff->data)
	{
		free(in->in_buff->data);
		in->in_buff->data = NULL;
		in->in_buff->size = 0;	
	}
	return nmemb;
}


void	get_market_history(t_market_list *list)
{
	CURL		*handle;
	t_history_data	*hist_buff;
       	CURLcode 	res;
	char		url[100];
	t_market_list 	*m;

	hist_buff = (t_history_data*)malloc(sizeof(t_history_data));
	hist_buff->in_buff = (input*)malloc(sizeof(input));
	hist_buff->in_buff->size = 0;
	hist_buff->in_buff->data = NULL;

	handle = curl_easy_init();
	if (!handle)
		return;
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, get_history); // set func callback
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, hist_buff); //set input buffer

	m = list;
	while (m)
	{
		m->hist.curr_idx = 0;
		hist_buff->market = m;

		memcpy(url, GETMARKETHISTORY, 60);
		memcpy(url + 60, m->name, m->name_sz);
		url[60 + m->name_sz] = '\0';
		printf("\n %s",url);
		curl_easy_setopt(handle, CURLOPT_URL, url);

		res = curl_easy_perform(handle);
		if (res != CURLE_OK)
			fprintf(stderr, "curl easy perform error\n", curl_easy_strerror(res));
		
		m->hist.first_chunk = 0;
		hist_buff->in_buff->size = 0;
		if (hist_buff->in_buff->data)
		{
			free(hist_buff->in_buff->data);
			hist_buff->in_buff->data = NULL;
			hist_buff->in_buff->size = 0;
		}
		bot_detect_duplicate_orders(m);
		m = m->next;

	}

	curl_easy_cleanup(handle);
	
	free(hist_buff->in_buff);
	free(hist_buff);

}


t_market_list *parce_markets(input *in)
{
	t_market_list *list;
	t_market_list *t;
	size_t	idx = 0;
	size_t	x;

	list = NULL;
	if (memcmp(in->data + 11, "true", 4) != 0)
		return (NULL);
	while (in->data[idx] != '[' && idx < in->size)
		idx++;
	
	if (idx == in->size)
		return (NULL);
	idx++;
	while (in->data[idx] != ']' && idx < in->size)
	{
		if (in->data[idx] == '{')
		{
			while (in->data[idx] != '}' && idx < in->size)
			{
				if (in->data[idx] == 'M' && strncmp(in->data + idx, "MarketName", 10) == 0)
				{
					idx += 13;
					
					if (memcmp(in->data + idx, "BTC", 3) == 0) //filter only btc markets 
					{	
						x = 0;
						while (in->data[idx + x] != '"')
							x++;
						if (memcmp(in->data + idx + x + 13, "true", 4) == 0) //filter only active markets
						{
							t = (t_market_list*)malloc(sizeof(t_market_list));
							t->next = list;
							list = t;
				
							t->name = (char*)malloc(sizeof(char) * x + 1);
							strncpy(t->name, in->data + idx, x);
							t->name[x] = '\0';
							t->name_sz = x;
							t->hist.last_tr_id = 0;
							t->hist.first_chunk = 0;
						}
					}
				}
				idx++;

			}
		}

		idx++;
	}

	return (list);
}

size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{

	char	*t;
	input	*in = (input*)userp;

	if (in == NULL || buffer == NULL)
		return 0;
	if (in->size == 0)
	{
		in->size = nmemb;
		in->data = (char*)malloc(sizeof(char) * nmemb);
		memcpy((void*)in->data, buffer, nmemb);
	}
	else
	{
		t = (char*)malloc(sizeof(char) * (in->size + nmemb));
		memcpy(t, in->data, in->size);
		memcpy(t + in->size, buffer, nmemb);
		free(in->data);
		in->size += nmemb;
		in->data = t;
	}
	return nmemb;
}


int main()
{	
	CURL	*handle;
       	CURLcode res;
	input	*buff;
	t_market_list *list;

	buff = (input*)malloc(sizeof(input));
	buff->size = 0;
	buff->data = NULL;
	handle = curl_easy_init();
	if (handle)
	{
		curl_easy_setopt(handle, CURLOPT_URL, GETMARKETS);
		curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(handle, CURLOPT_WRITEDATA, buff);
		res = curl_easy_perform(handle);
		if (res != CURLE_OK)
			fprintf(stderr, "curl easy perform error\n", curl_easy_strerror(res));
		curl_easy_cleanup(handle);
	}


	printf("\n\n%ld bytes received\n", buff->size);
	
	list = parce_markets(buff);
	free(buff->data);
	free(buff);
	if (!list)
		fprintf(stderr, "parce error\n", 1);
	
	get_market_history(list);
	
	printf("\n\n");

	t_market_list *t = list;
	while (t)
	{
		list = list->next;
		free(t);
		t = list;
	}
		
	return 0;
}

#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include "btrx.h"
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>


double	*get_sell_orders(input* buff)
{
	size_t	rate = 0;
	size_t	idx = 0;
	double	*sum;
	double	order_price = 0;
	double	order_quan = 0;
	double	price = 0;

	if (buff->data[11] != 't')
	{
		printf(" Failed request \n");
		return NULL;
	}
	
	sum = (double*)malloc(sizeof(double) * 5);
	memset(sum, 0, sizeof(double) * 5);

	idx = 50;
	while (buff->data[idx] && idx < buff->size)
	{
		if (buff->data[idx] == 'Q')
		{
			idx += 10;
			order_quan = atof(buff->data + idx);
			idx += 11;
			while (buff->data[idx] != 'R')
				idx++;
			idx += 6;
			order_price = atof(buff->data + idx);
			if (price == 0)
				price = order_price;
			if (rate < 4 && order_price > price + (price * ((rate + 1) * 0.25)))
			{
				//printf("rate %ld price %lf\n", rate + 1, order_price);	
				rate++;
				sum[rate] += sum[rate - 1];
			}
			sum[rate] += order_price * order_quan;
			idx += 11;	
		}	
		idx++;
	}
	if (rate != 4)
		sum[4] = sum[rate];
	return sum;
}


double	*get_buy_orders(input* buff)
{
	size_t	rate = 0;
	size_t	idx = 2;
	size_t	orders = 1;
	double	*sum;
	double	order_price = 0;
	double	order_quan = 0;
	double	price = 0;
	double	buy_wall = 0;
	
	if (buff->data[11] != 't')
	{
		printf(" Failed request \n");
		return NULL;
	}
	
	sum = (double*)malloc(sizeof(double) * 6);
	memset(sum, 0, sizeof(double) * 6);
	
	idx = 50;
	while (buff->data[idx] && idx < buff->size)
	{
		if (buff->data[idx] == 'Q')
		{
			idx += 10;
			order_quan = atof(buff->data + idx);
			idx += 11;
			while (buff->data[idx] != 'R')
				idx++;
			idx += 6;
			order_price = atof(buff->data + idx);
			if (price == 0)
				price = order_price;
			if (rate < 4 && order_price < price - price * ((rate + 1) * 0.25))
			{	
		 		//printf("rate %ld price %lf\n", rate + 1, order_price);
				rate++;
				sum[rate] += sum[rate - 1];
			}
			sum[rate] += order_price * order_quan;
			idx += 11;
			if (orders < 50)
			{
				if (order_price * order_quan > buy_wall)
					buy_wall = order_price * order_quan;
				orders++;
			}
			if (orders == 50)
			{
				if (buy_wall >= sum[rate] / 2 || buy_wall > 9.0)
					sum[5] = buy_wall;
				orders++;
			}

		}	
		idx++;
	}
	if (rate != 4)
		sum[4] = sum[rate];
	return sum;
}

size_t	write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{

	char	*t;
	input	*in = (input*)userp;

	if (in == NULL || buffer == NULL)
		return 0;
	if (in->size == 0)
	{
		in->size = nmemb;
		in->data = (char*)malloc(sizeof(char) * nmemb);
		memcpy((void*)in->data, buffer, nmemb);
	}
	else
	{
		t = (char*)malloc(sizeof(char) * (in->size + nmemb));
		memcpy(t, in->data, in->size);
		memcpy(t + in->size, buffer, nmemb);
		free(in->data);
		in->size += nmemb;
		in->data = t;
	}
	return nmemb;
}

void	wall_observer(int idx, double *bwall)
{
	static unsigned char	walls[MAX_TICKERS] = {0};

	if (*bwall == 0 && walls[idx] == 1)
	{
		printf("%s: %d removed \n", tickers[idx], (int)time(NULL));
		walls[idx] = 0;
	}
	else if (*bwall != 0 && walls[idx] == 0)
	{
		printf("%s[%lf]: %d new \n", tickers[idx], *bwall, (int)time(NULL));
		walls[idx] = 1;
	}
}

int	get_order_book(int idx, size_t len, CURL *handle, FILE *fd)
{
       	CURLcode	res;
	input		*buff;
	double		*buy = NULL;
	double		*sell = NULL;
	char 		url[150];
		
	strcpy(url, GETORDERBOOK);
	strcat(url, tickers[idx]);
	strcat(url, "&type=buy");
	
	
	buff = (input*)malloc(sizeof(input));
	buff->size = 0;
	buff->data = NULL;
		
	curl_easy_setopt(handle, CURLOPT_URL, url);
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, buff);
	res = curl_easy_perform(handle);
	if (res != CURLE_OK)
	{
		fprintf(stderr, "curl easy perform error\n");
		return (1);
	}
	buy = get_buy_orders(buff);
	free(buff->data);
	if (buy == NULL)
	{
		free(buff);
		return (1);
	}
	buff->data = NULL;
	buff->size = 0;

	strcpy(url + (ORDERB_LEN + len), "sell");
	curl_easy_setopt(handle, CURLOPT_URL, url);
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, buff);
	res = curl_easy_perform(handle);
	if (res != CURLE_OK)
	{
		free(buy);
		fprintf(stderr, "curl easy perform error\n");
		return (1);
	}
	sell = get_sell_orders(buff);
	free(buff->data);
	buff->data = NULL;
	free(buff);
	if (sell == NULL)
		return (1);

//	if (buy[0] > sell[0] || (sell[1] > 0 && buy[1] > sell[1]))
//		printf("\n\n          \033[1;32mB%s\033[0m: %d\n", market, (int)time(NULL));	
//	else
//		printf("\n\n          %s: %d\n", market, (int)time(NULL));

	
//	printf("BUY 25 [%lf] | 50 [%lf] | 75 [%lf] | 100 [%lf] | all [%lf]\n", buy[0], buy[1], buy[2], buy[3], buy[4]);	
//	printf("SELL 25 [%lf] | 50 [%lf] | 75 [%lf] | 100 [%lf] | all [%lf]\n\n", sell[0], sell[1], sell[2], sell[3], sell[4]);	
	if (buy[5] != 0)
	{	
		fprintf(fd, "*%d;", (int)time(NULL));	
		fprintf(fd, "%lf,%lf,%lf,%lf,%lf,%lf;", buy[0], buy[1], buy[2], buy[3], buy[4], buy[5]);
	}
	else
	{
		fprintf(fd, "%d;", (int)time(NULL));	
		fprintf(fd, "%lf,%lf,%lf,%lf,%lf;", buy[0], buy[1], buy[2], buy[3], buy[4]);
	}
	wall_observer(idx, buy + 5);
	fprintf(fd, "%lf,%lf,%lf,%lf,%lf\n", sell[0], sell[1], sell[2], sell[3], sell[4]);

	free(buy);
	free(sell);
	return 0;

}

int	main(int argc, char **argv)
{	
	CURL	*handle;
	size_t	ticker_len[MAX_TICKERS];
	size_t	idx = 0;
	char	market[30] = "./db/";
	FILE	*f[MAX_TICKERS];
	pid_t	pid = 0;

	pid = fork();
	if (pid > 0)
	{
		printf(" \n Child process %d\n", pid);
		return 1;
	}
	else if (pid == 0)
		printf("\n Child process running\n");
	else
		printf("\n fork error\n");
	

	while (1)
	{
		printf("\n");
		idx = 0;	
		while (idx < MAX_TICKERS)
		{
			ticker_len[idx] = strlen(tickers[idx]);
			strcpy(market + 5, tickers[idx]);
			strcat(market, ".txt");
			f[idx] = fopen(market, "a");
			if (f[idx] == NULL)
			{
				printf(" File can't be opened\n");
				return (1);
			}
			idx++;
		}
		
		handle = curl_easy_init();
		if (handle)
		{	
			idx = 0;
			while (idx < MAX_TICKERS)
			{
				get_order_book(idx, ticker_len[idx], handle, f[idx]);
				idx++;
			}
			curl_easy_cleanup(handle);
		}
	
		idx = 0;
		while (idx < MAX_TICKERS)
			fclose(f[idx++]);
	}

	return EXIT_SUCCESS;

}

__attribute__((destructor)) void del(void)
{
	

}
